package com.lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Lambda_2 {

	public static void main(String[] args) {

		List<Integer> lista = Arrays.asList(1, 5, 8, 9, 1, 4, 7, 6, 6, 9, 9);

		// forEach - opera��o final
		System.out.println("forEach - opera��o final");
		lista.stream() // cria um stream com todos os n�meros
				.limit(3).map(e -> e * 2) // dobra valor dos tr�s primeiros n�meros
				.forEach(e -> System.out.println(e));

		// count - opera��o final
		System.out.println("count - opera��o final");
		long count = lista.stream()
			.limit(9)
			.map(e -> e * 2) // dobra valor dos n�meros
			.count();
		System.out.println(count);
		
		System.out.println("count - opera��o final - vers�o 2");
		long count2 = lista.stream()
			.filter(e -> e % 2 == 0) 
			.count(); // conta n�meros pares
		System.out.println(count2);
		
		System.out.println("min - opera��o final");
		Optional<Integer> min = lista.stream()
			.filter(e -> e % 2 == 0) 
			.min(Comparator.naturalOrder()); 
		System.out.println(min.get());
		
		System.out.println("max - opera��o final");
		Optional<Integer> max = lista.stream()
			//.filter(e -> e % 2 == 0) 
			.max(Comparator.naturalOrder()); 
		System.out.println(max.get());
		
		System.out.println("collect - opera��o final");
		List<Integer> novaLista = lista.stream()
			.filter(e -> e % 2 == 0) 
			.collect(Collectors.toList()); 
		System.out.println(novaLista);
		
		System.out.println("collect - opera��o final");
		List<Integer> nLista = lista.stream()
			.filter(e -> e % 2 == 0) 
			.map(e -> e * 3)
			.collect(Collectors.toList()); 
		System.out.println(nLista);
		
		System.out.println("collect - opera��o final");
		Map<Boolean, List<Integer>> mapa = lista.stream()
			.map(e -> e * 3)
			.collect(Collectors.groupingBy(e -> e % 2 == 0)); 
		System.out.println(mapa);
		
		//Agrupa por resto da divis�o: resto zero, 1 ou 2
		System.out.println("collect - opera��o final");
		Map<Integer, List<Integer>> mapa1 = lista.stream()
			.collect(Collectors.groupingBy(e -> e % 3)); 
		System.out.println(mapa1);
		
		System.out.println("joining - opera��o final - junta tudo numa string");
		String collect = lista.stream()
			.map(e -> String.valueOf(e)) 
			.collect(Collectors.joining()); 
		System.out.println(collect);
		
		System.out.println("joining - opera��o final - junta tudo separado por ; numa string");
		String collect1 = lista.stream()
			.map(e -> String.valueOf(e)) //Converte cada elemento da List em String
			.collect(Collectors.joining(";")); //Junta todos em uma string
		System.out.println(collect1);
		
		/* Tr�s Collectors: count, min e max
		 * toList armeazena todos em uma lista
		 * groupingBy cria um mapa para agrupar elementos por qualquer crit�rio
		 * joining para concatenar strings
		 * 
		 * Streams = loops impl�citos
		 * for - while - do..while = loops expl�citos*/
	}

}
