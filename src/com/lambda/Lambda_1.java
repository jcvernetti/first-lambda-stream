package com.lambda;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Lambda_1 {

	public static void main(String[] args) {

		List<Integer> lista = Arrays.asList(1, 5, 8, 9, 1, 4, 7, 6, 6, 9, 9);

		// Java 4
		System.out.println("JAVA 4");
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}

		// Java 5
		System.out.println("JAVA 5");
		for (Integer integer : lista) {
			System.out.println(integer);
		}

		// Java 8
		// stream = fluxo de dados
		System.out.println("JAVA 8");
		lista.stream().forEach(e -> System.out.println(e));

		// stream: fluxo de opera��es intermedi�rias
		System.out.println("1 - skip(2)");
		lista.stream().skip(2) // Ignora os dois primeiros dados
				.forEach(e -> System.out.println(e));

		System.out.println("2 - limit(2)");
		lista.stream() // cria um stream com todos os n�meros
				.limit(2) // Considera os dois primeiros e ignora o restante dos dados
				.forEach(e -> System.out.println(e));

		System.out.println("3 - distinct()");
		lista.stream() // cria um stream com todos os n�meros
				.distinct() // Ignora dados repetidos (equals hashcode)
				.forEach(e -> System.out.println(e));

		System.out.println("4 - skip(2) e limit(3)");
		lista.stream() // cria um stream com todos os n�meros
				.skip(2) // pula dois
				.limit(3) // s� os tr�s pr�ximos
				.forEach(e -> System.out.println(e));

		// skip, limit e distinct s�o tipos de filtros
		System.out.println("5 - filter n�meros pares");
		lista.stream() // cria um stream com todos os n�meros
				.filter(e -> e % 2 == 0) // s� n�meros pares
				.forEach(e -> System.out.println(e));

		// Transforma��o de dados - map
		System.out.println("6 - map:  Transforma��o de dados");
		lista.stream() // cria um stream com todos os n�meros
				.distinct().map(e -> e * 2) // dobra valor de todos os n�meros
				.forEach(e -> System.out.println(e));
		System.out.println("A lista n�o � alterada");
		System.out.println(lista);
	}

}
